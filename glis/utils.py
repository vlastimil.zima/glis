#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Utilities used in glis."""
from functools import lru_cache

from gitlab import Gitlab
from gitlab.v4.objects import Project


@lru_cache
def get_project(gl: Gitlab, project_id: int) -> Project:
    """Get project from id."""
    return gl.projects.get(project_id)
