#!/usr/bin/python3
"""Print merge requests from gitlab."""
from typing import Dict, List, Optional, Union, cast

from gitlab import Gitlab
from gitlab.v4.objects import CurrentUser, Group, MergeRequest, Project, ProjectMergeRequest
from rich.columns import Columns
from rich.console import Console, Group as _Group
from rich.table import Table
from rich.text import Text

from .utils import get_project

COLOR_MAP = {
    '#5843AD': 'cyan',
    '#8E44AD': 'magenta',
    '#69D100': 'green',
}


def merge_requests(*, all_users: bool, group: Optional[str] = None) -> None:
    """Print merge requests from gitlab."""
    gl = Gitlab.from_config()
    gl.auth()

    project_mrs: Dict[Project, List] = {}
    if group is None:
        source: Union[Gitlab, Group] = gl
    else:
        source = gl.groups.get(group)
    for mr in source.mergerequests.list(state='opened', all=True, with_labels_details=True, scope='all'):
        if not mr.assignee:
            continue
        if not all_users \
                and cast(CurrentUser, gl.user).id not in (mr.assignee['id'], ) + tuple(r['id'] for r in mr.reviewers):
            continue

        project = get_project(gl, mr.project_id)
        if project.archived:
            continue
        mrs = project_mrs.setdefault(project, [])
        mrs.append(mr)
    # Sort by project name
    project_mrs = dict(sorted(project_mrs.items(), key=lambda x: cast(str, x[0].name_with_namespace.lower())))
    _print_mrs(project_mrs)


def _print_mrs(project_mrs: Dict[Project, List[Union[MergeRequest, ProjectMergeRequest]]]) -> None:
    console = Console()
    for project, mrs in project_mrs.items():
        # Not an actual table, only a way to render the MRs.
        table = Table(title=project.name_with_namespace, show_lines=True, show_header=False)
        table.add_column("merge_request")
        for mr in mrs:
            labels = ['[' + COLOR_MAP.get(j['color'], 'white') + ']' + j['name'] for j in mr.labels]
            if mr.has_conflicts:
                labels.append('[red]CONFLICT')

            # XXX: Reload the merge request from project. As of python-gitlab 2.10 API differs.
            mr = project.mergerequests.get(mr.iid)

            milestone = ''
            if mr.milestone:
                milestone = mr.milestone['title']

            if mr.approvals.get().approved:
                labels.insert(0, '[green bold]APPROVED[/green bold]')

            reviewers = [r['name'] for r in mr.reviewers]
            content = _Group(
                Columns((mr.title, Text(milestone, justify='right')), expand=True),
                Columns((' '.join(labels), Text('Assignee: ' + mr.assignee['name'], justify='right')), expand=True),
                Columns((mr.web_url, Text('Reviewers: ' + ' '.join(reviewers), justify='right')), expand=True))
            table.add_row(content)
        console.print(table)
