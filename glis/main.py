"""Main command structure."""
from typing import Optional

import click

from glis.merge_requests import merge_requests
from glis.milestones import milestones


@click.group()
def main() -> None:
    """glis: Tools for Gitlab Issues and Stuff."""


@main.command()
@click.option('--all-users', is_flag=True, default=False)
@click.option('--group', type=str, default=None)
def mr(all_users: bool, group: Optional[str]) -> None:
    """Print merge requests from gitlab."""
    merge_requests(all_users=all_users, group=group)


@main.command()
@click.option('--closed', is_flag=True, default=False, help="Show milestones in closed states as well.")
@click.option('--group', type=str, default=None, help="Show group milestones")
def milestone(closed: bool, group: Optional[str]) -> None:
    """Print milestones from gitlab."""
    milestones(group=group, closed=closed)


if __name__ == '__main__':
    main()
