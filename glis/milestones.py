#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Print milestones from gitlab."""
from typing import Dict, List, Optional, Union, cast

from gitlab import Gitlab
from gitlab.exceptions import GitlabListError
from gitlab.v4.objects import Group, Project
from rich.columns import Columns
from rich.console import Console, Group as _Group
from rich.table import Table
from rich.text import Text

from .utils import get_project


def milestones(*, group: Optional[str] = None, closed: bool = False) -> None:
    """Print milestones from gitlab."""
    gl = Gitlab.from_config()
    gl.auth()

    if group is None:
        source = gl.projects.list(archived=False, membership=True, all=True)
    else:
        source = [gl.groups.get(group)]

    pr_milestones: Dict[Union[Project, Group], List] = {}
    for subsource in source:
        try:
            mil = subsource.milestones.list(all=True)
        except GitlabListError:
            # Most probably forbidden...
            continue
        if not closed:
            mil = [m for m in mil if m.state == 'active']
        if mil:
            if group is None:
                project: Union[Project, Group] = get_project(gl, mil[0].project_id)
            else:
                project = gl.groups.get(mil[0].group_id)

            mils = pr_milestones.setdefault(project, [])
            mils.extend(mil)

    if group:
        pr_milestones = dict(sorted(pr_milestones.items(), key=lambda x: cast(str, x[0].name.lower())))
    else:
        pr_milestones = dict(sorted(pr_milestones.items(), key=lambda x: cast(str, x[0].name_with_namespace.lower())))
    _print_milestones(pr_milestones)


def _print_milestones(pr_milestones: Dict[Union[Project, Group], List]) -> None:
    console = Console()
    for project, milestones in pr_milestones.items():
        if isinstance(project, Project):
            name = project.name_with_namespace
        else:
            name = project.name
        table = Table(title=name, show_lines=True, show_header=False)
        table.add_column("milestones")
        for mil in milestones:
            if mil.due_date is not None:
                if mil.expired:
                    due_date = Text(f'Due date: {mil.due_date}', style='bold red', justify='right')
                else:
                    due_date = Text(f'Due date: {mil.due_date}', style='bold green', justify='right')
            else:
                due_date = Text('Due date: N/A', justify='right')
            content = _Group(
                Columns((mil.title, Text(mil.state, justify='right')), expand=True),
                Columns((mil.description,)),
                Columns((mil.web_url, due_date), expand=True),
            )
            table.add_row(content)
        console.print(table)
