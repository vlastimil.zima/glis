from unittest import TestCase

from click.testing import CliRunner

from glis.main import main


class MainTest(TestCase):
    runner = CliRunner(mix_stderr=False)

    def test_main(self):
        result = self.runner.invoke(main)

        self.assertEqual(result.exit_code, 0)
        self.assertIn('Usage:', result.stdout)
        self.assertEqual(result.stderr, '')
