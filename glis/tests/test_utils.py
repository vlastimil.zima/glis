from unittest import TestCase

import responses
from gitlab import Gitlab
from gitlab.base import RESTManager
from gitlab.v4.objects import Project

from glis.utils import get_project


class GetProjectTest(TestCase):
    """Unittests for get_project."""

    def setUp(self):
        self.gitlab = Gitlab('https://localhost')
        self.manager = RESTManager(self.gitlab)

    def test_get_missing(self):
        with responses.RequestsMock() as res:
            res.add(responses.GET,
                    'https://localhost/api/v4/projects/1',
                    json={})
            self.assertIsNone(get_project(self.gitlab, 1).get_id())

    def test_get_present(self):
        with responses.RequestsMock() as res:
            res.add(responses.GET,
                    'https://localhost/api/v4/projects/1',
                    json={'id': 1})
            self.assertEqual(get_project(self.gitlab, 1), Project(manager=self.manager, attrs={'id': 1}))
